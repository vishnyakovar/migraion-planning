#pragma once
#include <torch/torch.h>
#include <iostream>
#include "state.hpp"
#include "CQNCR.cpp"
#include "simulation.cpp"
#include "logging.cpp"

namespace {

using PreparedFeaturs = std::tuple<std::vector<float>, std::vector<int>>;

static constexpr long FEATURE_COUNT = 8;

struct NetImpl : torch::nn::Module {
  NetImpl() {
    torch::manual_seed(1);
    fc1 = register_module("fc1", torch::nn::Linear(FEATURE_COUNT, 32));
    fc2 = register_module("fc2", torch::nn::Linear(FEATURE_COUNT, 32));
    encoder = register_module("encoder", torch::nn::GRU(torch::nn::GRUOptions(32, 48).batch_first(true)));
    transcoder = register_module("transcoder", torch::nn::Linear(48, 48));
    decoder = register_module("decoder", torch::nn::GRU(torch::nn::GRUOptions(32, 48).batch_first(true)));
    estimator = register_module("estimator", torch::nn::Linear(48, 1));
  }

  torch::Tensor forward(const PreparedFeaturs &pf) {
    std::vector<float> features = std::get<0>(pf);
    const std::vector<int> &action_codes = std::get<1>(pf);
    std::vector<float> action_features;
    action_features.reserve(action_codes.size() * FEATURE_COUNT);
    if (action_codes[0] == -1) {
      action_features.resize(FEATURE_COUNT, 0);
    }
    for (uint i = 0; i < features.size(); i += FEATURE_COUNT) {
      if (features[i] > 0) {
        action_features.insert(action_features.end(), features.begin() + i, features.begin() + i + FEATURE_COUNT);
      }
    }

    torch::Tensor ft = torch::from_blob(features.data(), {1, static_cast<long>(features.size() / FEATURE_COUNT), FEATURE_COUNT});
    torch::Tensor actions = torch::from_blob(action_features.data(), {1, static_cast<long>(action_features.size() / FEATURE_COUNT), FEATURE_COUNT});
    ft = torch::relu(fc1->forward(ft));
    ft = std::get<1>(encoder->forward(ft));
    ft = torch::relu(transcoder->forward(ft));
    actions = torch::relu(fc2->forward(actions));
    ft = std::get<0>(decoder->forward(actions, ft));
    ft = estimator->forward(ft);
    return {ft};
  }

private:

  // Use one of many "standard library" modules.
  torch::nn::Linear fc1{nullptr}, fc2{nullptr}, transcoder{nullptr}, estimator{nullptr};
  torch::nn::GRU encoder{nullptr}, decoder{nullptr};
};

template <typename RndGen>
std::tuple<int, float> e_greedy(const torch::Tensor &q_values, RndGen rndgen, double epsilon = 0.03) {
  auto a = q_values.accessor<float, 3>();
  float max = std::numeric_limits<float>::min();
  int chosen = 0;
  for (uint i = 0; i < a.size(1); ++i) {
    float cur = a[0][i][0];
    if (max < cur) {
      max = cur;
      chosen = i;
    }
  }
  if (std::bernoulli_distribution(epsilon)(rndgen)) {
    return {std::uniform_int_distribution(0l, a.size(1) - 1)(rndgen), max};
  } else {
    return {chosen, max};
  }
}

template <typename NetworkType>
PreparedFeaturs prepare_input(State<NetworkType> &state) {
  state.set_to_e();
  std::vector<uint> migrate_to_this(state.hosts.size());
  for (const MigrationTask &i : state.planned_migrations) {
    migrate_to_this[i.to] += i.viable && !i.active;
  }
  std::vector<float> features;
  std::vector<int> action_codes;

  for (auto &i : state.planned_migrations) {
    if (i.active) {
      action_codes.emplace_back(ACTION_WAIT);
      break;
    }
  }

  for (uint i = 0; i < state.vms.size(); ++i) {
    const MigrationTask &task = state.planned_migrations[i];
    if (!task.viable) {
      continue;
    }

    const VM &vm = state.vms[i];
    float bandwidth = state.network.residual_throughput(task.from, task.to);
    float time_left = (1 - task.completion) * vm.ram / std::max(1e-4, (bandwidth - vm.dirty_rate));
    bool able_to_start = state.able_to_start(i);

    features.emplace_back(able_to_start);
    features.emplace_back(task.active);
    features.emplace_back(task.completion);
    features.emplace_back(vm.dirty_rate);
    features.emplace_back(vm.ram);
    features.emplace_back(state.hosts[task.to].ram);
    features.emplace_back(time_left);
    features.emplace_back(migrate_to_this[task.from]);

    if (able_to_start) {
      action_codes.emplace_back(i);
    }
  }
  return {features, action_codes};
}

TORCH_MODULE(Net);

}

class NeuralTrain {
public:
  Net net;
  torch::optim::Adam optim;
  torch::Tensor prev_result;
  double prev_estimation = -1;
  float prev_bt;

  NeuralTrain(Net net): net(net), optim(net->parameters()) {
    net->train(true);
  }

  template <typename NetworkType>
  void decide(State<NetworkType> &state) {
    while (true) {
      int action = choose_action(state);
      if (action == ACTION_WAIT) {
        return;
      }
      state.start_migration(action);
    }
  }

  void step() {
    optim.step();
    optim.zero_grad();
    prev_estimation = -1;
  }

private:

//  template <typename NetworkType>
//  int choose_action(State<NetworkType> &state) {
//    // Todo: what a mess;
//    PreparedFeaturs features = prepare_input(state);
//    auto codes = std::get<1>(features);
//
//    if (codes.size() == 1) {
//      return codes[0];
//    }
//    float reward = prev_estimation - time_to_finish_simplest<CQNCRSimple>(state);
////    float reward = prev_estimation - state.passed_time;
////    float reward = -prev_bt;
////    reward *= -1;
//    float max_value = 0;
//
//    int res;
//    torch::Tensor cur_result;
//    if (codes.empty()) {
//      res = ACTION_WAIT;
//    } else {
//      auto q_values = net->forward(features);
//      auto [chosen, max_value_real] = e_greedy(q_values, state.rndgen, 0.05);
//      max_value = max_value_real;
//      cur_result = q_values.index({0, chosen, 0});
//      res = codes[chosen];
//    }
//    if (prev_estimation != -1) {
//      float target = reward + 0.99 * max_value;
//      prev_result -= target;
//      auto loss = torch::square(prev_result);
//      loss.backward();
//    }
//    prev_estimation -= reward;
//    prev_result = std::move(cur_result);
//    prev_bt = -1e9;
//    auto raw_features = std::get<0>(features);
//    for (uint i = 6; i < raw_features.size(); i += FEATURE_COUNT) {
//      prev_bt = std::max(prev_bt, raw_features[i]);
//    }
//    return res;
//  }

  template <typename NetworkType>
  int choose_action(State<NetworkType> &state) {
    // Todo: what a mess;
    PreparedFeaturs features = prepare_input(state);
    auto codes = std::get<1>(features);

    if (codes.size() == 1) {
      return codes[0];
    }
    float reward = prev_estimation - time_to_finish_simplest<CQNCRSimple>(state);
//    float reward = prev_estimation - state.passed_time;
//    float reward = -prev_bt;
//    reward *= -1;
    float max_value = 0;

    int res;
    torch::Tensor cur_result;
    if (codes.empty()) {
      res = ACTION_WAIT;
    } else {
      auto q_values = net->forward(features);
      auto [chosen, max_value_real] = e_greedy(q_values, state.rndgen, 1);
      max_value = max_value_real;
      cur_result = q_values.index({0, chosen, 0});
      res = codes[chosen];
//      res = codes[std::uniform_int_distribution<int>(0l, codes.size() - 1)(state.rndgen)];
    }
//    if (prev_estimation != -1) {
//      float target = reward + 0.99 * max_value;
//      prev_result -= target;
//      auto loss = torch::square(prev_result);
//      loss.backward();
//    }
    prev_estimation -= reward;
    prev_result = std::move(cur_result);
    prev_bt = -1e9;
    auto raw_features = std::get<0>(features);
    for (uint i = 6; i < raw_features.size(); i += FEATURE_COUNT) {
      prev_bt = std::max(prev_bt, raw_features[i]);
    }
    return res;
  }

};

class NeuralApply {
public:
  Net net;

  NeuralApply(Net net): net(net) {
    net->train(false);
  }

  template <typename NetworkType>
  void decide(State<NetworkType> &state) {
    torch::NoGradGuard no_grad;
    while (true) {
      int action = choose_action(state);
      if (action == ACTION_WAIT) {
        return;
      }
      state.start_migration(action);
    }
  }

  void step() {}

private:

  template <typename NetworkType>
  int choose_action(State<NetworkType> &state) {
    PreparedFeaturs features = prepare_input(state);
    auto codes = std::get<1>(features);
    if (codes.empty()) {
      return ACTION_WAIT;
    }
    return codes[std::uniform_int_distribution<int>(0l, codes.size() - 1)(state.rndgen)];
//    if (codes.size() == 1) {
//      return codes[0];
//    }
//    auto q_values = net->forward(features);
//    auto [chosen, max_value_real] = e_greedy(q_values, state.rndgen, 0);
//    return codes[chosen];
  }
};