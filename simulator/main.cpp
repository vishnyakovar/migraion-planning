#include <iostream>
#include <chrono>
#include "state.hpp"
#include "strategies/CQNCR.cpp"
#include "strategies/MCTS.cpp"
#include "strategies/neural.cpp"
#include "contrib/thread_pool.hpp"
#include "simulation.cpp"

/*
 * Units: GB, sec, cores: i. e. net is in GB/s
 */

template<typename Network>
double estimate_sequential(const State<Network> &state) {
  double res = 0;
  for (int i = 0; i < state.planned_migrations.size(); ++i) {
    if (state.planned_migrations[i].viable) {
      res += state.vms[i].ram / (1.25 * 0.7 - state.vms[i].dirty_rate_scale);
    }
  }
  return res;
}

template <typename Strategy=CQNCRSimple>
std::string evaluate_single(int seed, int vm_count) {
  auto begin = std::chrono::steady_clock::now();
  std::stringstream res;
  State<TwoTierNetwork> state = synthetic_state(vm_count, seed);
  Strategy strategy;
  double bound = estimate_sequential(state);
  if (bound == 0) {
    return "None";
  }
  try {
    double value = time_to_finish(state, state.rndgen, strategy);
    res << "[" << value << ", "
        << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count()
        << "]";
    return res.str();
  } catch (const char *msg) {
    return "\"" + std::string(msg) + "\"";
  }
}

std::string evaluate_single_fast(int seed) {
  auto begin = std::chrono::steady_clock::now();

  std::stringstream res;
  CQNCRSimple strategy;
  State<> state = synthetic_state(100, seed);
  double bound = estimate_sequential(state);
  if (bound == 0) {
    return "None";
  }
//  res << bound << ' ' << time_to_finish(state, state.rndgen, strategy) << '\n';
  double res_time;
  try {
    res_time = time_to_finish_formula(state, state.rndgen, strategy);
  } catch (const char *msg) {
    return std::string(msg);
  }

  res << "[" << res_time << ", "
      << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << "]";
  return res.str();
}

template <typename Strategy>
std::string train_nn(int seed, int vm_count, Strategy &strategy, std::deque<double> &window) {
  auto begin = std::chrono::steady_clock::now();
  std::stringstream res;
  State<TwoTierNetwork> state = synthetic_state(vm_count, seed);
  double bound = estimate_sequential(state);
  if (bound == 0) {
    return "None";
  }
  try {
    double baseline = time_to_finish_simplest<CQNCRSimple>(state);
    double value = time_to_finish(state, state.rndgen, strategy);
    strategy.step();

    window.push_back(value / baseline);
    if (window.size() > 200) {
      window.pop_front();
    }
    double sum = 0;
    for (double i : window) {
      sum += i;
    }

    res << "[" << value / baseline << ", "
        << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count()
        << ", " << sum / window.size() << ", " << window.size()
        << "]";
    return res.str();
  } catch (const char *msg) {
    return "\"" + std::string(msg) + "\"";
  }
}


template <typename Strategy>
std::string apply_nn(int seed, int vm_count, Strategy &strategy) {
  auto begin = std::chrono::steady_clock::now();
  std::stringstream res;
  State<TwoTierNetwork> state = synthetic_state(vm_count, seed);
  double bound = estimate_sequential(state);
  if (bound == 0) {
    return "None";
  }
  try {
    double value = time_to_finish(state, state.rndgen, strategy);
    res << "[" << value << ", "
        << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count()
        << "]";
    return res.str();
  } catch (const char *msg) {
    return "\"" + std::string(msg) + "\"";
  }
}

std::string evaluate_nn(int seed, int vm_count) {
  Net network;
  torch::load(network, "/home/artemiyfly/Documents/project/model3.pt");
  NeuralApply strategy(network);
  std::deque<double> window;
  return train_nn(seed, vm_count, strategy, window);
}

// Main sync
//int main() {
//  auto begin = std::chrono::steady_clock::now();
//  for (uint i = 0; i < 4; ++i) {
//    std::cout << evaluate_single<CQNCR>(i, 30) << ",\n";
//  }
//  std::cerr << "\n\n" << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << '\n';
//}


// Main parallel
//int main() {
//  thread_pool pool;
//
//  auto begin = std::chrono::steady_clock::now();
//
//  std::vector<std::future<std::string>> futures(200);
//  for (uint i = 0; i < futures.size(); ++i) {
//    futures[i] = pool.submit(evaluate_single<MCTS<TwoTierNetwork>>, i, 100);
//  }
//  std::cout << "[\n";
//  for (uint i = 0; i < futures.size(); ++i) {
//    if (i > 0) {
//      std::cout << ",\n";
//    }
//    std::cout << "  " << futures[i].get();
//  }
//  std::cout << "\n]\n";
//  std::cerr << "\n\n" << std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - begin).count() << '\n';
//}


// Torch
//int main() {
//  Net network;
////  torch::load(network, "/home/artemiyfly/Documents/project/model3.pt");
//  NeuralTrain strategy(network);
//  std::deque<double> window;
//  for (uint i = 0; i < 10000; ++i) {
//    std::cout << train_nn(i, 100, strategy, window) << ",\n";
//    if (i % 100 == 0) {
//      torch::save(network, "/home/artemiyfly/Documents/project/model4.pt");
//    }
//  }
//}

int main() {
  Net network;
  torch::load(network, "/home/artemiyfly/Documents/project/model4.pt");
  NeuralTrain strategy(network);
  std::deque<double> window;
  std::cout << "[\n";
  for (uint i = 0; i < 200; ++i) {
    if (i > 0) {
      std::cout << ",\n";
    }
    std::cout << "  " << apply_nn(i, 100, strategy);
  }
  std::cout << "\n]\n";
}
