#pragma once

#include <vector>
#include "migration_task.h"
#include "host.cpp"


struct Link {
  const HostId v1;
  const HostId v2;
  const double full_capacity;
  double residual_capacity = 0;

  template<typename RndGen>
  void tick(RndGen &rndgen) {
    residual_capacity = std::max(0., std::normal_distribution<double>(full_capacity * 0.7, full_capacity * 0.15)(rndgen));
    residual_capacity = std::min(residual_capacity, full_capacity);
  }

  void set_to_e() {
    residual_capacity = 0.7 * full_capacity;
  }
};

struct NetworkBase {
  std::vector<Link> links;

  template<typename RndGen>
  void tick(RndGen &rndgen) {
    for (Link &link : links) {
      link.tick(rndgen);
    }
  }

  void set_to_e() {
    for (Link &link : links) {
      link.set_to_e();
    }
  }
};

struct FlatNetwork : public NetworkBase {
  void create(const std::vector<Host> &hosts) {
    links.clear();
    links.reserve(hosts.size());
    for (const Host &h : hosts) {
      links.emplace_back(Link{h.id, 10000, 1.25});
    }
  }

  void pass_trough(int src_pos, int dst_pos, double load) {
    links[src_pos].residual_capacity -= load;
    links[dst_pos].residual_capacity -= load;
  }

  double residual_throughput(int src_pos, int dst_pos) {
    return std::min(links[src_pos].residual_capacity, links[dst_pos].residual_capacity);
  }
};


struct TwoTierNetwork : public NetworkBase {
  int n_per_rack;
  int total_hosts;

  /*
   * [host -> top_of_rack for total_hosts]
   * [top_of_rack -> bus for racks]
   */

  void create(const std::vector<Host> &hosts) {
    links.clear();
    n_per_rack = std::min((int) floor(sqrt(hosts.size())), 42);
    total_hosts = hosts.size();
    for (uint i = 0; i < hosts.size(); ++i) {
      links.emplace_back(Link{hosts[i].id, 10001 + i / n_per_rack, 1.25});
    }
    int racks = (total_hosts - 1) / n_per_rack + 1;
    for (int i = 0; i < racks; ++i) {
      links.emplace_back(Link{10001 + i, 10000, 5});
    }

  }

  double residual_throughput(int src_pos, int dst_pos) {
    double res = std::min(links[src_pos].residual_capacity, links[dst_pos].residual_capacity);

    int src_rack = src_pos / n_per_rack;
    int dst_rack = src_pos / n_per_rack;
    if (src_rack != dst_rack) {
      res = std::min(res, links[src_rack + total_hosts].residual_capacity);
      res = std::min(res, links[dst_rack + total_hosts].residual_capacity);
    }

    return res;
  }

  void pass_trough(int src_pos, int dst_pos, double load) {
    int src_rack = src_pos / n_per_rack;
    int dst_rack = src_pos / n_per_rack;
    if (src_rack != dst_rack) {
      links[src_rack + total_hosts].residual_capacity -= load;
      links[dst_rack + total_hosts].residual_capacity -= load;
    }

    links[src_pos].residual_capacity -= load;
    links[dst_pos].residual_capacity -= load;
  }
};