#pragma once

#include "vm.cpp"

// 0-10000 hosts, 10000+ - network nodes
using HostId = int;

struct Host {
  HostId id;
  double cpu;
  double ram;

  bool fit(const VM &vm) const {
    return cpu >= vm.cpu_limit && ram >= vm.ram;
  }

  void place(const VM &vm) {
    cpu -= vm.cpu_limit;
    ram -= vm.ram;
  }
};

constexpr Host EMPTY_HOST{-1, 128, 768};