#pragma once

#include <vector>
#include "state.hpp"

constexpr double MOVING_PENALTY = 60;

namespace CycleResolving {

enum VertexState {
  UNVISITED,
  PROCESSING,
  PROCESSED,
  CYCLE_BEGIN
};

constexpr int MAX_VERTEX = 1e9;
constexpr int NOT_FOUND = -1;

/**
 * @param adjacency - adjacency list, with VM ids
 * @param state
 * @return id of lowest in cycle, NOT_FOUND otherwise
 */
template<typename NetworkType>
int run_dfs(const std::vector<std::vector<int>> &adjacency, const State<NetworkType> &state) {
  std::vector<VertexState> vertices_states(adjacency.size(), UNVISITED);
  for (uint i = 0; i < adjacency.size(); ++i) {
    if (vertices_states[i] == UNVISITED) {
      int report = dfs(i, adjacency, state, vertices_states);
      if (report != NOT_FOUND) {
        return report;
      }
    }
  }
  return NOT_FOUND;
}

template<typename NetworkType>
int dfs(
int vertex,
const std::vector<std::vector<int>> &adjacency,
const State<NetworkType> &state,
std::vector<VertexState> &vertices_states)
{
  vertices_states[vertex] = PROCESSING;
  for (int mig_id : adjacency[vertex]) {
    HostId target = state.planned_migrations[mig_id].to;
    if (vertices_states[target] == PROCESSED) {
      continue;
    }
    if (vertices_states[target] == PROCESSING) {
      vertices_states[target] = CYCLE_BEGIN;
      return mig_id + MAX_VERTEX;
    }
    int report = dfs(target, adjacency, state, vertices_states);
    if (report == NOT_FOUND) {
      continue;
    }
    if (report < MAX_VERTEX) {
      return report;
    }
    int min_migration;
    if (state.vms[mig_id].ram < state.vms[report - MAX_VERTEX].ram) {
      min_migration = mig_id + MAX_VERTEX;
    } else {
      min_migration = report;
    }
    if (vertices_states[vertex] == CYCLE_BEGIN) {
      return min_migration - MAX_VERTEX;
    } else {
      return min_migration;
    }
  }
  vertices_states[vertex] = PROCESSED;
  return NOT_FOUND;
}

template <typename NetworkType>
double resolve_cycles(State<NetworkType> &state) {
  double penalty = 0;
  int hosts_count = state.hosts.size();
  int vms_count = state.vms.size();
  while (true) {
    std::vector<bool> has_outgoing(state.hosts.size());
    for (int i = 0; i < vms_count; ++i) {
      if (state.planned_migrations[i].active) {
        has_outgoing[state.planned_migrations[i].from] = true;
      }
    }
    std::vector<std::vector<int>> adjacency(hosts_count);
    for (int i = 0; i < vms_count; ++i) {
      const MigrationTask &task = state.planned_migrations[i];
      if (task.viable
          && !task.active
          && !has_outgoing[task.to]
          && !state.have_resources(i)) {
        adjacency[task.from].emplace_back(i);
      }
    }

    int report = CycleResolving::run_dfs(adjacency, state);
    if (report == CycleResolving::NOT_FOUND) {
      break;
    }
    if (state.planned_migrations[report].to == state.hosts.size() - 1 ||
      state.planned_migrations[report].from == state.hosts.size() - 1) {
      throw "CycleWithSwapHost";
    }
//    if (!state.hosts.back().fit(state.vms[report])) {
//      throw "GiveUp";
//    }
    state.planned_migrations[report].next = state.planned_migrations[report].to;
    state.planned_migrations[report].to = hosts_count - 1;
    penalty += MOVING_PENALTY;
  }
  return penalty;
}


}