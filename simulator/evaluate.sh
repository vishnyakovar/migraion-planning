set -e

DATE=$(date "+%d_%H_%M")
TARGET=84.201.141.47

#rsync -rz contrib/libtorch $TARGET:

cmake --build cmake-build-debug --target simulator -- -j 6
git add -u
git commit -m "Experiment $DATE started"
scp cmake-build-debug/simulator $TARGET:
ssh $TARGET "LD_LIBRARY_PATH=$LD_LIBRARY_PATH:libtorch/lib time ./simulator > saved"
scp $TARGET:saved ../experiments/experiment_$DATE.json
