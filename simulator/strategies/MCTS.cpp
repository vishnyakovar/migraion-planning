#pragma once

#include <memory>
#include "state.hpp"
#include "simulation.cpp"
#include "strategies/CQNCR.cpp"

namespace {

template <typename NetworkType>
struct Node {
  int action;
  State<NetworkType> state;
  std::vector<std::unique_ptr<Node>> children;
  double qvalue_sum = 0;
  int times_visited = 0;
  Node *parent;

  Node(State<NetworkType> &&state, double time, int action, Node *parent): action(action), state(state), parent(parent) {
    this->state.passed_time = time;
  }

  bool is_leaf() const {
    return children.empty();
  }

  double estimate_qvalue() const {
    return times_visited ? qvalue_sum / times_visited : 0;
  }

  double ucb_score() const {
    if (times_visited) {
      double U = sqrt(2 * log(parent->times_visited) / times_visited);
      return estimate_qvalue() + U * 75;  // Tweak option: exploration score
    } else {
      return 1e100;
    }
  }

  Node *select_best_leaf() {
    if (is_leaf()) {
      return this;
    }
    Node *res;
    double res_score = -1e100;
    for (const std::unique_ptr<Node> &i : children) {
      if (i->ucb_score() > res_score) { // Tweak option: >= / >
        res_score = i->ucb_score();
        res = i.get();
      }
    }
    return res->select_best_leaf();
  }

  void expand() {
    for (auto &i : state.planned_migrations) {
      if (i.active) {
        State<NetworkType> awaited_state = state;
        double new_time = state.passed_time;
        new_time += CycleResolving::resolve_cycles(awaited_state);
        new_time += await_single_operation(awaited_state, awaited_state.rndgen); // Not completely correct
        children.emplace_back(std::make_unique<Node>(std::move(awaited_state), new_time, ACTION_WAIT, this));
        break;
      }
    }

    for (uint i = std::max(0, action + 1); i < state.planned_migrations.size(); ++i) {
      if (state.able_to_start(i)) {
        State<NetworkType> new_state = state;
        new_state.start_migration(i);
        children.emplace_back(std::make_unique<Node>(std::move(new_state), state.passed_time, i, this));
      }
    }
  }

  double rollout(double shift = 0) const {
    State<NetworkType> temp_state = state;
    CQNCRSimple strategy;
    try {
      return shift - state.passed_time - time_to_finish_formula(temp_state, temp_state.rndgen, strategy); // Tweak option: algorithm
    } catch (const char *msg) {
      return -1e9;
    }

  }

};

}

template <typename NetworkType>
class MCTS {
  void plan_mcts(Node<NetworkType> &root, int iterations, double shift) {
    for (int i = 0; i < iterations; ++i) {
      Node<NetworkType> *node = root.select_best_leaf();
      node->expand();
      if (!node->children.empty()) {
        // Tweak option: priority
        uint child_id = std::uniform_int_distribution<uint>(0, node->children.size() - 1)(node->state.rndgen);
        node = node->children[child_id].get();
      }
      double value = node->rollout(shift);
      while (node != nullptr) {
        ++(node->times_visited);
        node->qvalue_sum += value;
        node = node->parent;
      }
    }
  }

public:
  void decide(State<NetworkType> &state) {
    // Tweak option: wait for selected to finish
    std::unique_ptr<Node<NetworkType>> root(std::make_unique<Node<NetworkType>>(State<NetworkType>(state), 0, -2, nullptr));
    double default_score = -root->rollout() + 200;
    while (true) {
      if (root->children.empty()) {
        root->expand();
      }
      int best_child = -1;
      if (root->children.size() == 1) {
        best_child = 0;
      } else {
        plan_mcts(*root, 40, default_score); // Tweak option: iterations
        double best_score = -1e100;

        for (uint i = 0; i < root->children.size(); ++i) {
          double cur_score = root->children[i]->estimate_qvalue();
          if (cur_score > best_score) {
            best_score = cur_score;
            best_child = i;
          }
        }
      }
      if (best_child == -1) {
        // TODO: does actually finishing?
        return;
      }
      root = std::move(root->children[best_child]);
      root->parent = nullptr;
      if (root->action == ACTION_WAIT) {
        return;
      }
      state.start_migration(root->action);
    }
  }
};