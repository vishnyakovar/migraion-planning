#pragma once
#include "host.cpp"
#include "vm.cpp"

struct MigrationTask {
  HostId from;
  HostId to;
  double completion = 0;
  bool active = false;
  bool viable = false;
  HostId next = -1;
};