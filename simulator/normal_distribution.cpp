#pragma once

#include <random>

template<typename RealType>
std::normal_distribution<RealType> operator+(
    std::normal_distribution<RealType> &d1,
    std::normal_distribution<RealType> &d2)
{
    return std::normal_distribution<RealType>(d1.mean + d2.mean, hypot(d1.stddev, d2.stddev));
}