#pragma once

#include <fstream>
#include <chrono>
#include <iostream>

template <typename Value>
void log(const std::string& signal, Value value) {
  std::cerr << std::chrono::steady_clock::now().time_since_epoch().count() << '\t' << signal << '\t' << value << std::endl;
}
