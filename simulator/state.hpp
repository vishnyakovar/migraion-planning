#pragma once
#include <contrib/random/xorshift_wrapper.hpp>
#include <iostream>
#include <algorithm>
#include <sstream>

#include "network.cpp"

constexpr uint8_t MAX_CONCURRENT = 2;
constexpr int ACTION_WAIT = -1;

std::vector<HostId> ffd(const std::vector<VM> &vms, std::vector<Host> &hosts);

template<typename NetworkType=TwoTierNetwork>
struct State {
  XorShift128 rndgen;
  std::vector<Host> hosts;
  NetworkType network;
  std::vector<MigrationTask> planned_migrations;
  std::vector<VM> vms;
  std::vector<uint8_t> active_migrations;
  double passed_time = 0;

  /// Iterate random variables
  void tick() {
    network.tick(rndgen);
    for (VM &vm : vms) {
      vm.tick(rndgen);
    }
  }

  /// Set random variables to E
  void set_to_e() {
    network.set_to_e();
    for (VM &vm : vms) {
      vm.set_to_e();
    }
  }

  void start_migration(int id) {
#ifdef DEBUG_PRINTS
    std::cerr << "START_MIGRATION " << id << '\n';
#endif
    planned_migrations[id].active = true;
    hosts[planned_migrations[id].to].ram -= vms[id].ram;
    ++active_migrations[planned_migrations[id].to];
    ++active_migrations[planned_migrations[id].from];
  }

  void finish_migration(int id) {
#ifdef DEBUG_PRINTS
    std::cerr << "FINISH_MIGRATION " << id << '\n';
#endif
    planned_migrations[id].active = false;
    hosts[planned_migrations[id].from].cpu += vms[id].cpu_limit;
    hosts[planned_migrations[id].from].ram += vms[id].ram;
    hosts[planned_migrations[id].to].cpu -= vms[id].cpu_limit;
    --active_migrations[planned_migrations[id].to];
    --active_migrations[planned_migrations[id].from];

    if (planned_migrations[id].next == -1) {
      planned_migrations[id].viable = false;
    } else {
      planned_migrations[id] = MigrationTask{
        planned_migrations[id].to,
        planned_migrations[id].next,
        0,
        false,
        true,
        -1};
    }
  }

  bool have_resources(int id) const {
    return (hosts[planned_migrations[id].to].ram >= vms[id].ram
           && hosts[planned_migrations[id].to].cpu >= vms[id].cpu_limit)
           || planned_migrations[id].to == hosts.size() - 1; // TODO: get rid of
  }

  bool able_to_start(int id) const {
    const MigrationTask &task = planned_migrations[id];
    return task.viable
           && !task.active
           && have_resources(id)
           && active_migrations[task.to] < MAX_CONCURRENT
           && active_migrations[task.from] < MAX_CONCURRENT;
  }

  void print_progress_line() const {
    for (int i = 0; i < vms.size(); ++i) {
      if (!planned_migrations[i].viable) {
        std::cerr << "_ ";
      } else if (planned_migrations[i].active) {
        std::cerr << int(planned_migrations[i].completion * 10) << ' ';
      } else if (able_to_start(i)) {
        std::cerr << "O ";
      } else if (have_resources(i)) {
        std::cerr << "W ";
      } else {
        std::cerr << "B ";
      }
    }
    std::cerr << '\n';
  }

  std::string generate_report() const __attribute__((used)) __attribute__((noinline)){
    std::stringstream res;
    res << "-----[ REPORT AT " << passed_time << " ]-----\n";
    double total_ram = 0;
    for (auto host : hosts) {
      total_ram += host.ram;
    }
    res << "Free ram: " << total_ram << "\n";
    res << "Active migrations:";
    for (uint i = 0; i < planned_migrations.size(); ++i) {
      if (planned_migrations[i].active) {
        res << ' ' << i;
      }
    }
    res << '\n';
    return res.str();
  }
};

template<typename NetworkType=TwoTierNetwork>
State<NetworkType> synthetic_state(int vm_count, int seed = 0) {
  State<NetworkType> res{};
  res.rndgen.seed(seed);

  res.vms.reserve(vm_count);
  for (size_t i = 0; i < vm_count; ++i) {
    res.vms.push_back(sample_VM(res.rndgen));
  }

  std::vector<HostId> target = ffd(res.vms, res.hosts);
  std::vector<uint> priority(res.hosts.size());
  for (int i = 0; i < res.hosts.size(); ++i) {
    res.hosts[i].ram = (EMPTY_HOST.ram - res.hosts[i].ram) * 1.1;
    res.hosts[i].cpu = (EMPTY_HOST.cpu - res.hosts[i].cpu) * 1.2;
    priority[i] = i;
  }

  std::vector<HostId> source(vm_count, 0);
  for (int i = 0; i < vm_count; ++i) {
    std::shuffle(priority.begin(), priority.end(), res.rndgen);
    int dst = -1;
    for (int candidate : priority){
      if (res.hosts[candidate].fit(res.vms[i])) {
        dst = candidate;
        break;
      }
    }
    if (dst == -1) {
      dst = res.hosts.size();
      res.hosts.push_back(EMPTY_HOST);
      res.hosts[dst].id = dst;
    }
    res.hosts[dst].place(res.vms[i]);
    source[i] = dst;
  }

  res.planned_migrations.resize(vm_count);
  for (int i = 0; i < vm_count; ++i) {
    if (source[i] != target[i]) {
      res.planned_migrations[i] = MigrationTask{source[i], target[i], 0, false, true};
    } else {
      res.planned_migrations[i].from = res.planned_migrations[i].to = source[i];
      res.planned_migrations[i].viable = false;
    }
  }

  res.hosts.push_back(EMPTY_HOST);
  res.hosts.back().id = res.hosts.size() - 1;
  res.active_migrations.resize(res.hosts.size());

  res.network.create(res.hosts);
  return res;
}