#pragma once

#include "state.hpp"
#include "simulation.cpp"

namespace {

// TODO:
// Start next migrations if possible

struct RIG {
  std::vector<bool> used_links;
  std::vector<bool> used_hosts;
  std::vector<int> migrations;

  RIG(const State<FlatNetwork> &state): used_links(), used_hosts(state.hosts.size(), false), migrations() {}
  RIG(const State<TwoTierNetwork> &state): used_links(state.hosts.size(), false), used_hosts(state.hosts.size(), false), migrations() {}

  void register_links(const State<FlatNetwork> &state, int id) { (void)state; (void)id; }
  void register_links(const State<TwoTierNetwork> &state, int id) {
    int fr = state.planned_migrations[id].from / state.network.n_per_rack;
    int tr = state.planned_migrations[id].to / state.network.n_per_rack;
    if (fr != tr) {
      used_links[fr] = true;
      used_links[tr] = true;
    }
  }
};


bool fit(const RIG &rig, int migration_id, const State<FlatNetwork> &state) {
  auto &migration = state.planned_migrations[migration_id];
  return rig.used_hosts[migration.from] || rig.used_hosts[migration.to];
}

bool fit(const RIG &rig, int migration_id, const State<TwoTierNetwork> &state) {
  auto &migration = state.planned_migrations[migration_id];
  if (rig.used_hosts[migration.from] || rig.used_hosts[migration.to]) {
    return false;
  }
  int rack_from = migration.from / state.network.n_per_rack;
  int rack_to = migration.from / state.network.n_per_rack;
  return rack_from != rack_to ||
    !(rig.used_links[rack_from] || rig.used_links[rack_to]);
}


template <typename Implementation>
class CQNCRBase
{
public:
  template <typename Network>
  void decide(State<Network> &state) {
    for (auto &i : state.planned_migrations) {
      if (i.active) {
        return;
      }
    }
    std::vector<RIG> rigs;
    for (uint i = 0; i < state.planned_migrations.size(); ++i) {
      if (!state.planned_migrations[i].viable) {
        continue;
      }

      int fitting = -1;
      for (uint j = 0; j < rigs.size(); ++j) {
        if (fit(rigs[j], i, state)) {
          fitting = j;
          break;
        }
      }
      if (fitting == -1) {
        fitting = rigs.size();
        rigs.emplace_back(state);
      }
      rigs[fitting].migrations.push_back(i);
      rigs[fitting].used_hosts[state.planned_migrations[i].from] = true;
      rigs[fitting].used_hosts[state.planned_migrations[i].to] = true;
      rigs[fitting].register_links(state, i);
    }

    if (rigs.empty()) {
      return;
    }
    int id_max = Implementation::best_group(rigs, state);
    for (int i : rigs[id_max].migrations) {
      state.start_migration(i);
    }
  }
};

struct EmptyStrategy {
  template <typename Network>
  void decide(State<Network> &state) {}
};

}


class CQNCRSimple: public CQNCRBase<CQNCRSimple> {
public:
  template <typename Network>
  static int best_group(std::vector<RIG> &rigs, State<Network> &state) {
    int id_max = -1;
    for (uint i = 0; i < rigs.size(); ++i) {
      if (id_max == -1 || rigs[id_max].migrations.size() < rigs[i].migrations.size()) {
        id_max = i;
      }
    }
    return id_max;
  }
};

class CQNCR: public CQNCRBase<CQNCR> {
public:
  template <typename Network>
  static int best_group(std::vector<RIG> &rigs, State<Network> &state) {
    int id_max = -1;
    double lowest_score = std::numeric_limits<double>::max();
    for (uint i = 0; i < rigs.size(); ++i) {
      double score = evaluate_group(rigs, state, i);
      if (lowest_score > score) {
        lowest_score = score;
        id_max = i;
      }
    }
    return id_max;
  }

private:
  template <typename Network>
  static double evaluate_group(std::vector<RIG> &rigs, State<Network> state, uint id) {
    for (auto &task : state.planned_migrations) {
      task.viable = false;
      task.next = -1;
    }
    EmptyStrategy emptyStrategy;
    for (int i : rigs[id].migrations) {
      state.planned_migrations[i].viable = true;
      state.start_migration(i);
    }
    double self_time = time_to_finish_formula(state, state.rndgen, emptyStrategy);
    double sum_others = 0;
    for (uint j = 0; j < rigs.size(); ++j) {
      if (j == id) {
        continue;
      }
      State<Network> temp_state = state;
      for (int i : rigs[j].migrations) {
        state.planned_migrations[i].viable = true;
        state.start_migration(i);
      }
      sum_others += time_to_finish_formula(state, state.rndgen, emptyStrategy);
    }
    return self_time * 1.4 + sum_others / rigs.size();
  }
};

