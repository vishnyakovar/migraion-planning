#pragma once

#include <vector>
#include <algorithm>


#include "cycles_resolving.cpp"
#include "state.hpp"

static constexpr double HARD_TL = 1e5;

template <typename NetworkType, typename RndGen>
double await_single_operation(State<NetworkType> &state, RndGen &rndgen) {
  state.set_to_e();
  std::vector<double> bandwidths = allocate_bandwidth(state, rndgen, 60);
  double fastest = 1e10;
  for (uint i = 0; i < state.vms.size(); ++i) {
    if (state.planned_migrations[i].active) {
      double speed = std::max(1e-20, (bandwidths[i] - state.vms[i].dirty_rate) / state.vms[i].ram);
      double left = (1 - state.planned_migrations[i].completion) / speed;
      if (left < fastest) {
        fastest = left;
      }
    }
  }
  // TODO: proper handle of all un-continuable

  for (uint i = 0; i < state.vms.size(); ++i) {
    if (state.planned_migrations[i].active) {
      double speed = std::max(1e-20, (bandwidths[i] - state.vms[i].dirty_rate) / state.vms[i].ram);
      state.planned_migrations[i].completion += speed * fastest;
      if (state.planned_migrations[i].completion >= 1) {
        state.finish_migration(i);
      }
    }
  }
  return fastest;
}

template<typename NetworkType, typename RndGen, typename Strategy>
double time_to_finish_formula(State<NetworkType> &state, RndGen &rndgen, Strategy &strategy) {
  double total_time = 0;
  while (true) {
    bool finished = true;
    for (auto &i : state.planned_migrations) {
      if (i.viable) {
        finished = false;
        break;
      }
    }
    if (finished) {
      break;
    }
    strategy.decide(state);
    total_time += CycleResolving::resolve_cycles(state);
    total_time += await_single_operation(state, rndgen);
  }
  return total_time;
}

template<typename Strategy, typename NetworkType>
double time_to_finish_simplest(State<NetworkType> state) {
  Strategy strategy;
  XorShift128 rndgen = state.rndgen;
  return time_to_finish_formula(state, rndgen, strategy);
}

template<typename NetworkType, typename RndGen>
std::vector<double> allocate_bandwidth(State<NetworkType> &state, RndGen &rndgen, int n_passes) {
  std::vector<double> bandwidths(state.vms.size(), 0);
  std::vector<int> order(state.vms.size());
  for (uint i = 0; i < order.size(); ++i) {
    order[i] = i;
  }
  std::shuffle(order.begin(), order.end(), rndgen);
  for (int pass = 0; pass < n_passes; ++pass) {
    for (int i : order) {
      if (!state.planned_migrations[i].active) {
        continue;
      }
      auto &task = state.planned_migrations[i];
      double limit = state.network.residual_throughput(task.from, task.to);
      double provisioned = limit / (n_passes - pass);
      state.network.pass_trough(task.from, task.to, provisioned);
      bandwidths[i] += provisioned;
    }
  }
  return bandwidths;
}

// tick-based

constexpr double TICK_TIME = 1e-1;


template<typename NetworkType, typename RndGen, typename Strategy>
double time_to_finish(State<NetworkType> &state, RndGen &rndgen, Strategy &strategy) {
  double start_time = state.passed_time;
//  double prev_report = 0;
  state.passed_time += CycleResolving::resolve_cycles(state);
  strategy.decide(state);
  while (state.passed_time < HARD_TL) {
//    if (total_time - prev_report > 100) {
//      state.print_progress_line();
//      prev_report = total_time;
//    }
    // circle resolve
    state.passed_time += TICK_TIME;
    state.tick();
    // Two passes with random priorities
    // Can me viewed as 2 packets executed each tick;
    int N_PASSES = 2;
    std::vector<double> provisioned = allocate_bandwidth(state, rndgen, N_PASSES);

    for (uint i = 0; i < state.vms.size(); ++i) {
      auto &task = state.planned_migrations[i];
      if (!task.active) {
        continue;
      }
      double bandwidth = std::max(0., provisioned[i] - state.vms[i].dirty_rate);
      task.completion += bandwidth * TICK_TIME / state.vms[i].ram;
      if (task.completion >= 1) {
        state.finish_migration(i);
        state.passed_time += CycleResolving::resolve_cycles(state);

#ifdef DEBUG_PRINTS
        state.print_progress_line();
        std::cerr << i << " done\n";
#endif
        strategy.decide(state);
#ifdef DEBUG_PRINTS
        state.print_progress_line();
#endif
        bool finished = true;
        for (const MigrationTask &j : state.planned_migrations) {
          if (j.viable) {
            finished = false;
            break;
          }
        }
        if (finished) {
          return state.passed_time - start_time;
        }
      }
    }
  }
  return HARD_TL;
}