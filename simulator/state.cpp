#pragma once

#include "vm.cpp"
#include "host.cpp"
#include <algorithm>
#include <iostream>
#include <vector>

std::vector<HostId> ffd(const std::vector<VM> &vms, std::vector<Host> &hosts) {
  std::vector<std::pair<double, uint>> weights;
  weights.reserve(vms.size());
  for (uint i = 0; i < vms.size(); ++i) {
    double weight = vms[i].cpu_limit / EMPTY_HOST.cpu + vms[i].ram / EMPTY_HOST.ram;
    weights.emplace_back(weight, i);
  }
  std::sort(weights.begin(), weights.end());
  std::vector<HostId> placement(vms.size());
  for (auto &i : weights) {
    int dst = -1;
    for (uint j = 0; j < hosts.size(); ++j) {
      if (hosts[j].fit(vms[i.second])) {
        dst = j;
        break;
      }
    }
    if (dst == -1) {
      dst = hosts.size();
      hosts.push_back(EMPTY_HOST);
      hosts[dst].id = dst;
    }
    hosts[dst].place(vms[i.second]);
    placement[i.second] = dst;
  }
  return placement;
}