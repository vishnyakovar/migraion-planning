#pragma once

#include <random>
using VMId = int;

struct VM {
  double cpu_limit;
  double ram;
  double dirty_rate_scale;

  double dirty_rate;

  template<typename RndGen>
  void tick(RndGen &rndgen) {
    dirty_rate = std::normal_distribution<double>(1, 0.5)(rndgen) * dirty_rate_scale;
  }

  void set_to_e() {
    dirty_rate = dirty_rate_scale;
  }
};

struct Flavor {
  double cpu;
  double ram;
};

const Flavor flavors[] = {
Flavor{2 * 0.5, 8},
Flavor{4 * 0.5, 16},
Flavor{8 * 0.5, 32},
Flavor{16 * 0.5, 64},
Flavor{32 * 0.5, 128},
Flavor{2 * 0.25, 16},
Flavor{4 * 0.25, 32},
Flavor{8 * 0.25, 64},
Flavor{16 * 0.25, 128},
Flavor{2 * 0.25, 2},
Flavor{4 * 0.25, 4},
Flavor{8 * 0.25, 8},
Flavor{16 * 0.25, 16},
Flavor{32 * 0.25, 32},
Flavor{64 * 0.25, 128},
Flavor{64 * 0.25, 64}
};

//TODO: proper
template<typename RndGen>
VM sample_VM(RndGen &rndgen) {
  Flavor f = flavors[std::uniform_int_distribution<>(0, sizeof(flavors) / sizeof(*flavors) - 1)(rndgen)];
  double avg_pdr = std::poisson_distribution<int>(4)(rndgen) * 0.012;
  // double avg_net_in = std::poisson_distribution<int>(5)(rndgen) * f.cpu * 2;
  // double avg_net_out = std::poisson_distribution<int>(5)(rndgen) * f.cpu * 3;
  return {
  f.cpu,
  f.ram,
  avg_pdr,
  0
  // std::normal_distribution<double>(avg_net_in, avg_net_in),
  // std::normal_distribution<double>(avg_net_out, avg_net_out),
  };
}